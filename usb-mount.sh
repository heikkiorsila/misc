#!/bin/bash
#
# A wrapper for mount command that can be used to mount sd* devices so that
# max_ratio for dirty pages is limited to only 10% of available memory.
#
# See https://www.kernel.org/doc/Documentation/ABI/testing/sysfs-class-bdi
#
# This is useful for USB devices when copying more files to the device than
# there is available RAM for dirty pages. When the RAM is filled with dirty
# pages, kernel may have to allocate memory by flushing dirty pages to USB
# device, which can be really slow, e.g. 5 MiB/s, and this makes applications
# really slow. To avoid this, max ratio for
# the mount is set to 10% which means that at most 10 percent of available
# memory for dirty pages is used for this device.
#
# USAGE:
#
# usb-mount.sh takes the same parameters as the mount command.

new_max_ratio="10"

process_opts="yes"
block_device=""
num_block_device="0"
for arg in "$@" ; do
    if [[ ${process_opts} = "yes" ]] ; then
	if [[ ${arg} = "--" ]] ; then
	    process_opts="no"
	    continue
	fi
	if [[ ${arg:0:1} = "-" ]] ; then
	    continue
	fi
    fi
    if [[ ! -e "${arg}" ]] ; then
	continue
    fi
    filetype=$(stat --format="%F" "${arg}" 2>/dev/null)
    if [[ "${filetype}" != "block special file" ]] ; then
	continue
    fi
    num_block_device=$((num_block_device + 1))
    if [[ ${num_block_device} -gt 1 ]] ; then
	echo "Already have a block device. Won't do set block device attributes."
	block_device=""
	break
    fi
    block_device="${arg}"
done

mount "$@"
ret=$?
if [[ ${ret} != 0 ]] ; then
    exit ${ret}
fi

if [[ -n "${block_device}" ]] ; then
    bname="$(basename "${block_device}")"
    if [[ -n $(echo "${bname}" |grep "^sd[a-z][0-9]\+") ]] ; then
	disk_id="$(echo "${bname}" |sed -e "s|^sd\(.\)\([0-9]\)|sd\1|")"
	max_ratio_file="/sys/block/${disk_id}/bdi/max_ratio"
	if [[ -e "${max_ratio_file}" ]] ; then
	    max_ratio=$(cat "${max_ratio_file}")
	    if [[ "${max_ratio}" -gt ${new_max_ratio} ]] ; then
		echo "Set ${max_ratio_file} to ${new_max_ratio}."
		echo ${new_max_ratio} > "${max_ratio_file}"
	    fi
	fi
    fi
fi

exit 0
