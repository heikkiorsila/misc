# misc

Miscellaneous scripts and tools.

# Content

## usb-mount.sh

A wrapper for mount command that can be used to mount sd* devices so that
max_ratio for dirty pages is limited to only 10% of available memory.

See https://www.kernel.org/doc/Documentation/ABI/testing/sysfs-class-bdi

This is useful for USB devices when copying more files to the device
than there is available RAM for dirty pages. When the RAM is filled
with dirty pages, kernel may have to allocate memory by flushing dirty
pages to USB device, which can be really slow, e.g. 5 MiB/s, and this
makes applications really slow. To avoid this, max ratio for the mount
is set to 10% which means that at most 10 percent of available memory
for dirty pages is used for this device.

usb-mount.sh takes the same parameters as the mount command.
